---
title: "Three Stamp Shuixian"
date: 2020-09-23T20:38:03Z
draft: false
---

## Reaching out for those aromas

Today I am writing a review based on two sessions of this magnificent
High Roast Three Stamp Shuixian.

The first thing that I notice out of the bag is an incredible aroma that
makes you drool and lingers in your nose. It is like smelling slightly burnt
sugar with some spice that makes you snort all the tea.

I am hoping just one thing and that is the aroma mentioned before to translate
to the liquor.

In the first session using my roasty muting teapot, the wendan, the first steeps
were incredibly creamy but not that aromatic at all, like a good shuixian but having
a really bold body.

As for the second session with my Jianshui workhorse it was not as creamy but definitely
more aromatic and the aftertaste was that intense that could get the aroma.

I love out of these shuixian how it can become somehow spicy like nutmeg and
coat your mouth warming the body, perfect for a cold morning.

The following steeps were a bit more light even when pushing it but kept up
being enjoyable for 5 steeps or so which is okay for how much I push it.

Going around and coming back to the room makes me notice how the aromas have
taken up the room which is something I always enjoy.

Next time I will use a porcelain gaiwan to get another perspective from it but
definitely feels more special than other shuixians I had.
