---
title: "Everlasting Love"
date: 2020-09-23T20:37:44Z
draft: false
---

## 2013 Guangdong Dry Storage Xiaguan Everlasting Love

### A tale of an iron cake

Hello there fellow fermentation fiends, this is your favourite sloth reporting
yet another puerh cake that was bought with my tears and sweat.

Today we have a lovely and not just because of the name
2013 Guangdong Dry Storage Xiaguan Everlasting Love Raw Pu Erh Cake by your
local honkongese dealer Jay.

As soon as I open the wrapper notice how hard the cake is, perfectly usable as
a frisbee. It appears to have nice and whole leaves easily ruined by my hard
pick when breaking a few chunks for my personal consumption.

The aroma of the dry leaves is intense, somehow sweet and lingering, something
that makes me wonder if it has rested enough but at least hoping that it will
translate into the cup.

A quick rinse makes the chunks open into small bits of leaves and releasing the
aromas and flavours into a dark yellow liquor, should not be too intense for me
at these hours.

When drinking the first steep I notice a light and fragant profile that coats
the mouth with a fading bitterness over the following sips and leaving a woody
note that I can only define as elegant. It is fresh almost like camphor but
without being minty, a smooth fruity aftertaste is going behind the scenes and
orchestrating an addictive session that evolves into a relaxed brewing and
keeps me focused during the next steeps.

The late steeps brews into a thicc soup that reminds me of a more fruity juice
and still enjoyable to push further.

While not being overly complex it has made me get interest to see how it can
evolve with a bit of resting and makes me happy that I got a cake as a sample.

The price is fair and interesting, it isnt really expensive and even if it needs
a bit of further aging to round the edges it still has a lot of fragance that
must be preservated so the initial Guangdong storage with the care of Jay has
made a bold choice to anyone that wants to experience a solid mid aged sheng.

This is it for today, stay tuned for more reviews and enjoy your tea.

Sloth out.
