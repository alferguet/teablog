---
title: "12-13 Raw Liu An"
date: 2020-09-23T20:38:15Z
draft: false
---

## As comfy as your usual SoL anime

### A personal favorite

For me this tea is the pinnacle of the comfiness.

The presentation is legendary, a basket made out of the bamboo with the tea
lightly compressed inside. And the best part is that you can brew the bamboo to
deepen the experience.

The leaves once broken are small and delicate, but have not noticed harsher flavour
when breaking them too much, should not care about it too much.

I have tried this tea in many vessels and clays but what suits it best is a big
steel pot that brings most of the flavours resulting in a lazy and comfy session.

The tea itself is not too delicate. You can first notice a light bitterness but
worry not as it carries a citric flavour that makes it really interesting.

The liquor is thick and has a good body, a smokyness in the background is
supporting the citric profile giving an awesome combination for me.

You can get some hongcha notes out of it like hay if you brew it lightly or more
bready notes but not the usual for me.

I think this could be also considered as fruity as for the later steeps where
you can get ripe fruit notes with some sweetness.

It isnt the most exciting tea but the combination makes it all for me becoming
really comfy and enjoyable so having a full basket really makes me relieve my
anxiety to not be able to get more.

Shoutout to Jay again for being able to source a new basket of my favourite tea
and if someone wants a sample of it would be happy to ship it.

Do not hesitate to get a as much as possible of a tea you really enjoy to
not run out of it if possible.

Once it is gone, it is.
